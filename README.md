CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

This project integrates EuPlatesc.ro into the Drupal Commerce payment and
checkout systems.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/commerce_euplatesc

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/commerce_euplatesc


REQUIREMENTS
------------

This module requires the following modules:

* Drupal Commerce - https://www.drupal.org/project/commerce


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-modules
   for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Commerce > Configuration > Payment Gateways.
    3. Add new payment gateway:

        * Select EuPlatesc Plugin.
        * Fill in the Merchant ID and Secret key provided with your EuPlatesc
          registration.


MAINTAINERS
-----------

 * Melinda Csog (adevms) - https://www.drupal.org/u/adevms
